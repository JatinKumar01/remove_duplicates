from openpyxl import load_workbook

file_path = './ExcelFiles/data.xlsx'
workbook = load_workbook(file_path)
worksheet = workbook.active

unique_values = set()

for row in worksheet.iter_rows(min_row=1, max_row=worksheet.max_row, min_col=1, max_col=1):
    cell_value = row[0].value
    if cell_value not in unique_values:
        unique_values.add(cell_value)

for row in worksheet.iter_rows(min_row=1, max_row=worksheet.max_row, min_col=1, max_col=1):
    row[0].value = None

for index, value in enumerate(unique_values, start=1):
    worksheet.cell(row=index, column=1, value=value)

cleaned_file_path = './ExcelFiles/output.xlsx'
workbook.save(cleaned_file_path)

print(f"Duplicates removed and file saved to {cleaned_file_path}")
