# Remove Duplicates from Excel Column

This Python script removes duplicates from a specific column in an Excel file.

## How to Use

1. Clone this repository to your local machine:

```bash
git clone https://gitlab.com/JatinKumar01/remove_duplicates
```

2. Navigate to the project directory:

```bash
cd yourproject
```

3. Make sure you have Python installed on your machine.

4. Install the required dependencies:

```bash
pip install -r requirements.txt
```

5. Run the Python script to remove duplicates from the Excel file:

```bash
python app.py

```

6. Check the cleaned Excel file in the same directory.

That's it! Duplicates should now be removed from the specified column in your Excel file.

# Author

Jatin Kumar

# License

This project is licensed under the MIT License - see the LICENSE file for details.